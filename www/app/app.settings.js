﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('SettingsHelper', SettingsHelper);

    SettingsHelper.$inject = [];
    function SettingsHelper() {
        var service = {
            BaseUrl: 'http://ffdevweb.com.br/projetos/guiafreesp',
            //BaseUrl: 'http://localhost/guiafreesp',
            StorageName: 'APPGEO',
            CurrentLanguage: 'pt-BR'
        };

        return service;
    }

}());