(function (app) {
    app.directive('select2', select2)
    select2.$inject = ['EmpresaService', 'ItemEmpresaService', '$rootScope', 'SettingsHelper'];
    function select2(EmpresaService, ItemEmpresaService, $rootScope, SettingsHelper) {
        return {
            restrict: 'A',
            link: function ($scope, $element) {
                $($element).select2();
                var selectteste = $($element);
                $($element).on("change", function(evt) {
                    $rootScope.loader = true;
                    $($element).blur();
                    var id = $(this).val();
                    id = id.replace("string:","");
                      EmpresaService.getById(id).then(function(res){
                        if(res.data.length > 0){ 
                          $scope.enderecoEmpresa = res.data[0].endereco;
                          $scope.telefoneEmpresa = res.data[0].telefone;
                          ItemEmpresaService.getAllByIdEmpresa(res.data[0].id).then(function(response){
                            $scope.itens = response.data;
                            for(var i = 0; i < $scope.itens.length; i++){
                              $scope.itens[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.itens[i].imagem;
                            }
                            $scope.boxPatrocionadores = false;
                            $scope.boxItem = true;

                          }).catch(function(err){
                            console.log(err);
                            $rootScope.loader = false;
                          });
                        }
                        $rootScope.loader = false;
                      }).catch(function(err){
                        console.log(err);
                        $rootScope.loader = false;
                      });
                }); 
            }
        }
    }
}(angular.module('app.core')))