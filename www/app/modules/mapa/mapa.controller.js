﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('MapaController', MapaController)

    MapaController.$inject = ['PatrocionadorService', '$rootScope', '$scope', '$state', 'localStorageService', 'SettingsHelper', 'EmpresaService', 'ItemEmpresaService'];
    function MapaController(PatrocionadorService, $rootScope, $scope, $state, localStorageService, SettingsHelper, EmpresaService, ItemEmpresaService) {
        $scope.empresas = [];
        $scope.itens = [];
        $scope.patrocionadores = [];
        $scope.empresa = {};
        $scope.selectDrop = {};
        $scope.boxPatrocionadores = true;
        $scope.boxItem = false;
        $scope.logout = logout;
        $scope.goHome = goHome;
        $scope.enderecoEmpresa = "";
        $scope.telefoneEmpresa = "";

        function goHome(){
          $state.go("home");
        }

        $rootScope.bkpLogin = false;

        var map;
        initialize();
        function initialize() {
          var bounds = new google.maps.LatLngBounds();
          var latlngbounds = new google.maps.LatLngBounds();
          var infowindow = new google.maps.InfoWindow();
          var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15
          });
          if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {
              var pos = { 
                lat: position.coords.latitude,
                lng: position.coords.longitude
              };
              map.setCenter(pos);
              var marker1 = new google.maps.Marker({
                position: new google.maps.LatLng(position.coords.latitude, position.coords.longitude),
                map: map
              }); 
              google.maps.event.addListener(marker1, 'click', (function (marker1) {
                return function () {
                  var content = "<h5>Você está aqui!</h5>";
                  infowindow.setContent(content);
                  infowindow.open(map, marker1);
                  $scope.itens = [];
                  $scope.boxItem = false;
                }
              })(marker1));
              bounds.extend(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
              map.fitBounds(bounds);
            });
          }
          //EmpresaService.getById($rootScope.idEmpresa).then(function(response){
          EmpresaService.getById(32).then(function(response){
            if (response.data.length > 0) {    
                $scope.empresas = response.data;
                for(var i = 0; i < $scope.empresas.length; i++){                
                  var image = new google.maps.MarkerImage(
                      SettingsHelper.BaseUrl + "/admin/images/" +$scope.empresas[i].imagem,
                      null,  
                      null, 
                      null,
                      new google.maps.Size(32, 32)
                  );  
                  var marker = new google.maps.Marker({
                    position: new google.maps.LatLng($scope.empresas[i].lat, $scope.empresas[i].lng),
                    title: $scope.empresas[i].nome,
                    map: map,
                    icon: image
                  }); 
                        var iwContent = ""+
                        "<div>"+
                          "<h5>"+$scope.empresas[i].nome+"</h5>"+
                        "</div>";
                  
                        infowindow.setContent(iwContent);
                        infowindow.open(map, marker);
                  google.maps.event.addListener(marker, 'click', (function (marker, i) {
                      return function () {

                        var iwContent = ""+
                        "<div>"+
                          "<h5>"+$scope.empresas[i].nome+"</h5>"+
                        "</div>";
                  
                        infowindow.setContent(iwContent);
                        infowindow.open(map, marker);
                      }

                  })(marker, i));
                  
                  bounds.extend(new google.maps.LatLng($scope.empresas[i].lat, $scope.empresas[i].lng));
                  map.fitBounds(bounds);
                }
              }
              $scope.empresa = response.data[0];
              ItemEmpresaService.getAllByIdEmpresa($scope.empresa.id).then(function(response){
                $scope.itens = response.data;
                for(var i = 0; i < $scope.itens.length; i++){
                  $scope.itens[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.itens[i].imagem;
                }
                $scope.boxPatrocionadores = false;
                $scope.boxItem = true;
              }).catch(function(err){
                console.log(err);
                $rootScope.loader = false;
              });
            $rootScope.loader = false;
          }).catch(function(data){
              console.log(data);
          });
        }

        function logout(){
            localStorageService.remove("guiafreesp");
            navigator.app.exitApp();
        }


    }

}());