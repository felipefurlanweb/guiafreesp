﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('HomeController', HomeController)

    HomeController.$inject = ['PatrocionadorService', '$rootScope', '$scope', '$state', 'localStorageService', 'SettingsHelper', 'EmpresaService', 'ItemEmpresaService'];
    function HomeController(PatrocionadorService, $rootScope, $scope, $state, localStorageService, SettingsHelper, EmpresaService, ItemEmpresaService) {
        $scope.empresas = [];
        $scope.patrocionadores = [];
        $scope.sliderSelect = sliderSelect;
        $scope.sair = sair;
        $scope.fechar = fechar;

        ///////////////////////////////////////////////////////////////////////////////////////////

        $rootScope.bkpLogin = false;

        EmpresaService.getAllBanners().then(function(response){
          if (response.data.length > 0) {    
            $scope.banners = response.data;
            for(var i = 0; i < $scope.banners.length; i++){
              $scope.banners[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.banners[i].imagem;
            }
          }
        }).catch(function(data){
            console.log(data);
        });

        EmpresaService.getAll().then(function(response){
          if (response.data.length > 0) {    
            $scope.empresas = response.data;
            for(var i = 0; i < $scope.empresas.length; i++){
              $scope.empresas[i].imagem2 = SettingsHelper.BaseUrl + "/admin/images/" + $scope.empresas[i].imagem2;
            }
          }
        }).catch(function(data){
            console.log(data);
        });

        PatrocionadorService.getAll().then(function(response){
          if (response.data.length > 0) {    
            $scope.patrocionadores = response.data;
            for(var i = 0; i < $scope.patrocionadores.length; i++){
              $scope.patrocionadores[i].imagem = SettingsHelper.BaseUrl + "/admin/images/" + $scope.patrocionadores[i].imagem;
            }
          }
        }).catch(function(data){
            console.log(data);
        });

        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        function sair(){
            localStorageService.remove("guiafreesp");
            $state.go("login");
        }

        function sliderSelect(item){
          $rootScope.idEmpresa = item.id;
          $state.go("mapa");
        }

        function fechar(){
            localStorageService.remove("guiafreesp");
            navigator.app.exitApp();
        }


    }

}());