﻿(function () {
    'use strict';
    angular
        .module('app')
        .controller('LoginController', LoginController)

    LoginController.$inject = ['$rootScope', 'UsuarioService', '$scope', 'LoginService', '$state', 'localStorageService', 'SettingsHelper'];
    function LoginController($rootScope, UsuarioService, $scope, LoginService, $state, localStorageService, SettingsHelper) {
        $scope.user = {};
        $scope.model = {};
        $scope.login = login;
        $scope.insert = insert;
        $scope.showBoxInsert = showBoxInsert;
        $scope.showBoxLogin = showBoxLogin;
        $scope.boxLogin = true;
        $scope.boxCadastro = false;
        $rootScope.loader = false;

        ///////////////////////////////////////////////////////////////////////////

        activate();

        //////////////////////////////////////////////////////////////////////////

        function activate() {

            $rootScope.bkpLogin = true;

            var teste = localStorageService.get("guiafreesp");
            if(teste != null){
                $state.go("home");
            }
        }

        //////////////////////////////////////////////////////////////////////////

        function showBoxInsert(){
            $scope.model = {};
            $scope.user = {};
            $scope.boxLogin = false;
            $scope.boxCadastro = true;
        }
        function showBoxLogin(){
            $scope.model = {};
            $scope.user = {};
            $scope.boxLogin = true;
            $scope.boxCadastro = false;
        }
        
        
        function login() {
            if($scope.model.email == undefined || $scope.model.senha == undefined){
                alert("Preencha os campos");
                return;
            }
            LoginService.login($scope.model).then(function(res){
                if (res.data.length > 0) {
                    localStorageService.set("guiafreesp", res.data[0].id);
                    $state.go('home');
                }else{
                    alert("Nenhum registro encontrado");
                }
                $scope.user = {};
                $scope.model = {};
            }).catch(function(err){
                alert("Algo deu errado! tente novamente");
            });
        }

        function insert(){
            if($scope.user.nome == undefined || $scope.user.email == undefined || $scope.user.senha == undefined){
                alert("Preencha os campos");
                return;
            }
            UsuarioService.insert($scope.user).then(function(res){
                if (res.data.length > 0) {
                    localStorageService.set("guiafreesp", res.data[0].id);
                    $state.go('home');
                }else{
                    alert("Algo deu errado! tente novamente");
                }
                $scope.user = {};
                $scope.model = {};
            }).catch(function(err){
                alert("Algo deu errado! tente novamente");
            });
        }

    }

}());