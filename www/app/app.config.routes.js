﻿(function () {
    'use strict';
    angular
        .module('app')
        .config(config);

    config.$inject = ['$stateProvider', '$urlRouterProvider'];
    function config($stateProvider, $urlRouterProvider) {
        
        $urlRouterProvider.otherwise('tela1');

        $stateProvider

            .state('tela1',
            {
                url: '/tela1',
                templateUrl: "app/modules/tela1/tela1.html",
                controller: "Tela1Controller",
                data: { pageTitle: '' },
                resolve: {

                }
            })

            .state('login',
            {
                url: '/login',
                templateUrl: "app/modules/login/login.html",
                controller: "LoginController",
                data: { pageTitle: 'Página de Login' },
                resolve: {

                }
            })
            .state('logout',
            {
                url: '/logout',
                controller: "LogoutController",
                controllerAs: 'logoutCtrl',
                resolve: {

                }
            })
            .state('home',
            {
                url: '/home',
                templateUrl: "app/modules/home/home.html",
                controller: "HomeController",
                data: { pageTitle: 'Home' },
                resolve: {
                    
                }
            })
            .state('usuarios',
            {
                url: '/usuarios',
                templateUrl: "app/modules/usuarios/usuarios.html",
                controller: "UsuariosController",
                controllerAs: 'usuariosCtrl',
                data: { pageTitle: 'Usuarios' },
                resolve: {

                }
            })

            .state('mapa',
            {
                url: '/mapa',
                templateUrl: "app/modules/mapa/mapa.html",
                controller: "MapaController",
                data: { pageTitle: 'Mapa' },
                resolve: {
                    
                }
            })

    }

    isAuth.$inject = ['LoginService','$q'];
    function isAuth(LoginService, $q) {
        var deferred = $q.defer();
        if (!LoginService.isAuthenticate()) {
            deferred.reject({isAuth: false});
        } else {
            deferred.resolve({ isAuth: true });
        }
        return deferred.promise;
    }

}());