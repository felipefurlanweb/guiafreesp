(function () {
    'use strict';
    angular
        .module('app')
        .factory('ItemEmpresaService', ItemEmpresaService);

    ItemEmpresaService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function ItemEmpresaService($q, SettingsHelper, $timeout, $http) {

      var service = {
          getAllByIdEmpresa: getAllByIdEmpresa
      };
      
      return service;

      function getAllByIdEmpresa(id) {
          var promise = null;
          promise = $http({
              method : 'POST',
              url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
              data: $.param({ id: "getAllItemByIdEmpresa", idEmpresa: id}),
              headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
          }).then(function(response){ 
              return response; 
          }).catch(function(error){
              return "error"+error;
          });
          return promise;
      }
  
    }

}());