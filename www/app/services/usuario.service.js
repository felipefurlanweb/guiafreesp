﻿(function () {
    'use strict';
    angular
        .module('app')
        .factory('UsuarioService', UsuarioService);

    UsuarioService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function UsuarioService($q, SettingsHelper, $timeout, $http) {
        var service = {
            getAll: getAll,
            insert: insert,
            getById: getById,
            update: update,
            remove: remove,
            search: search
            //isAuthenticate: isAuthenticate,
            //getLoginData: getLoginData
        };
        var usuarios = [];

        return service;

        //////////////////////////////////////////////////////////////////////////

        function getAll() {
            var promise = null;
            promise = $http.get(SettingsHelper.BaseUrl + "/api/Usuario/").then(
                function (response) { return response.data; }
            );
            return promise;
        }

        function getById(id) {
            var promise = null;
            promise = $http.get(SettingsHelper.BaseUrl + "/api/Usuario/"+id).then(
                function (response) { return response.data; }
            );
            return promise;
        }

        function update(model) {
            var promise = null;
            promise = $http.put(SettingsHelper.BaseUrl + "/api/Usuario/", model).then(
                function (response) { return response.data; }
            );
            return promise;
        }

        function remove(id) {
            var promise = null;
            promise = $http.delete(SettingsHelper.BaseUrl + "/api/Usuario/"+id).then(
                function (response) { return response.data; }
            );
            return promise;
        }

        function insert(model) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
                  data: $.param({ id: "usuarioAdd", nome: model.nome, email: model.email, senha: model.senha}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
        }

        function search(nome) {
            var promise = null;
            promise = $http.get(SettingsHelper.BaseUrl + "/api/Usuario/Search/"+nome).then(
                function (response) { return response.data; }
            );
            return promise;
        }


    }

}());