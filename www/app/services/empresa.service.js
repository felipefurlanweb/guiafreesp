(function () {
    'use strict';
    angular
        .module('app')
        .factory('EmpresaService', EmpresaService);

    EmpresaService.$inject = ['$q', 'SettingsHelper', '$timeout', '$http'];
    function EmpresaService($q, SettingsHelper, $timeout, $http) {

          var service = {
              getAll: getAll,
              getById: getById,
              getAllBanners: getAllBanners
          };

          return service;

          function getAllBanners() {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
                  data: $.param({ id: "getAllBanners"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getAll() {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
                  data: $.param({ id: "getAllEmpresas"}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

          function getById(id) {
              var promise = null;
              promise = $http({
                  method : 'POST',
                  url : SettingsHelper.BaseUrl + "/app/www/server/form.php",
                  data: $.param({ id: "getEmpresaById", idEmpresa: id}),
                  headers : {'Content-Type': 'application/x-www-form-urlencoded'} 
              }).then(function(response){ 
                  return response; 
              }).catch(function(error){
                  return "error"+error;
              });
              return promise;
          }

    }

}());